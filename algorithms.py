from helper import *


def generational(generations, populationNumber, number, equation, dictionary, numbersList, firstElements):
    """
    runs the generational algorithm
    :param generations:         the number of generations
    :param populationNumber:    the number of chromosomes in the population
    :param number:              the number of lines to be added
    :param equation:            a list of list containing every line of the equation
    :param dictionary:          the dictionary with the letter as the key and the index for the list as the value
    :param numbersList:         the starting list
    :param firstElements:       a list containing the first letters from every word
    :return:
        @bestIndividual     - the best individual that was found after running the algorithm
        @bestList           - the fitness of the best chromosome from each generation
    """
    population = generation(number, equation, dictionary, populationNumber, numbersList, firstElements)
    bestIndividual, totalFitness = evaluation(population)
    bestList = []
    for g in range(generations):
        bestList.append(bestIndividual.getFitness())
        newPopulation = []
        probabilities = defineProbability(population, totalFitness)
        for i in range(populationNumber):
            offspring = selection(population, probabilities)
            mutation(offspring, dictionary, firstElements)
            newPopulation.append(offspring)

        population = newPopulation
        bestIndividual, totalFitness = evaluation(population)

    return bestIndividual, bestList


def steadyState(generations, populationNumber, number, equation, dictionary, numbersList, firstElements):
    """
    runs the steady state algorithm
    :param generations:         the number of generations
    :param populationNumber:    the number of chromosomes in the population
    :param number:              the number of lines to be added
    :param equation:            a list of list containing every line of the equation
    :param dictionary:          the dictionary with the letter as the key and the index for the list as the value
    :param numbersList:         the starting list
    :param firstElements:       a list containing the first letters from every word
    :return:
        @bestIndividual     - the best individual that was found after running the algorithm
        @bestList           - the fitness of the best chromosome from each generation
    """
    population = generation(number, equation, dictionary, populationNumber, numbersList, firstElements)
    bestIndividual, totalFitness = evaluation(population)
    bestList = []
    for g in range(generations):
        probabilities = defineProbability(population, totalFitness)

        offspring = selection(population, probabilities)
        mutation(offspring, dictionary, firstElements)
        offspring.evaluate()
        worstIndividual, index = worst(population)

        if worstIndividual.getFitness() < offspring.getFitness():
            population[index] = offspring

        bestIndividual, totalFitness = evaluation(population)
        bestList.append(bestIndividual.getFitness())

        if bestIndividual.isSolution():
            break

    return bestIndividual, bestList
