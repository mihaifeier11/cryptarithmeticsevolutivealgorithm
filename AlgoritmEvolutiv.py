import time
from random import shuffle, randrange, random
from Cromozom import Cromozom
from parserFile import parserFile, parsWords, parserDict, calcFirst, prinFunction, makeGrafic, writeResultToFile
import numpy as np


def validate(solution, dictPositions, firstLetters):
    """
    transform a wrong permutation into a valid one

    :param solution: the solution to be validated
    :param dictPositions: the dictionary with the positions of letters
    :param firstLetters: the string with the first letters
    :return: None
    """
    check = False
    while not check:
        check = True
        for l in firstLetters:
            if solution[dictPositions[l]] == 0:
                check = False
                try:
                    solution[dictPositions[l] + 1], solution[dictPositions[l]] = solution[dictPositions[l]], solution[
                        dictPositions[l] + 1]
                except IndexError:
                    solution[dictPositions[l]], solution[0] = solution[0], solution[dictPositions[l]]


def init(nrLetters, firstLetters, words, dictPositions):
    """
    generate a initial population

    :param nrLetters: the number of letters
    :param firstLetters: the string with the first letters
    :param words: the list of words
    :param dictPositions: the dictionary with the positions of letters
    :return: None
    """
    pop = []
    index = 0
    while index < nrLetters:
        solution = [i for i in range(10)]
        shuffle(solution)
        validate(solution, dictPositions, firstLetters)
        c = Cromozom(solution)
        c.eval(words, dictPositions)
        pop.append(c)
        index += 1
    return pop


def evaluate(pop, words, dictPositions):
    """

    :param pop: the list of solutions
    :param words:  the list of words
    :param dictPositions: the dictionary with the positions of letters
    :return: None
    """
    for individ in pop:
        individ.eval(words, dictPositions)


def selectie(population):
    """
    select a chromosome from the population

    :param population: the list of permutations
    :return: a chromosome
    """
    pop = list(population)
    ind1 = pop[randrange(len(pop))]
    ind2 = pop[randrange(len(pop))]
    if ind1.fit < ind2.fit:
        return ind1
    return ind2


def crossover(m, t):
    """
    crossover 2 chromosomes

    :param m: the mother
    :param t: the father
    :return: the new offset
    """
    mPerm = m.perm
    tPerm = t.perm

    offPerm = [-1] * 10
    n = randrange(10)
    for index in range(n):
        offPerm[index] = mPerm[index]
    index = 0
    for j in range(10):
        if tPerm[j] not in offPerm:
            offPerm[n + index] = tPerm[j]
            index += 1
    off = Cromozom(offPerm)
    return off


def mutation(off, dictPositions, first):
    """

    :param off: the offset
    :param dictPositions: the dictionary with the positions of letters
    :param first: the string with the first letters
    :return: None
    """
    changes = 2
    for i in range(changes):
        a = randrange(10)
        b = randrange(10)
        off.perm[a], off.perm[b] = off.perm[b], off.perm[a]
    validate(off.perm, dictPositions, first)


def bestPop(pop):
    """
    find the best chromosome

    :param pop: the list of permutations
    :return: the best chromosome
    """
    best = pop[0]
    for individ in pop:
        if individ.fit < best.fit:
            best = individ
    return best


def worstPop(pop):
    """
    find the worst chromosome

    :param pop: the list of permutations
    :return: the worst chromosome
    """
    worst = 0
    for i in range(len(pop)):
        if pop[i].fit > pop[worst].fit:
            worst = i
    return worst


def Generational(sizePop, nrIterations, words, dictPositions, first):
    """
    Algoritm Gnerational

    :param sizePop: the size of the population
    :param nrIterations: the number of the iterations
    :param words: the list of words
    :param dictPositions:  the dictionary with the positions of letters
    :param first: the string with the first letters
    :return:
    """
    pop = init(sizePop, first, words, dictPositions)
    k = 0
    best = bestPop(pop)
    index = 0
    points = []
    while best.fit != 0:
        index += 1
        newPop = []
        nrInd = 0
        while nrInd < len(pop) and best.fit != 0:
            m = selectie(pop)
            t = selectie(pop)
            off = crossover(m, t)
            mutation(off, dictPositions, first)
            off.eval(words, dictPositions)
            ok = True
            for ind in newPop:
                if ind.perm == off.perm:
                    ok = False
            if ok == True:
                newPop.append(off)
                nrInd += 1

        print(best.fit)
        points.append(best.fit)
        pop = newPop
        best = bestPop(pop)
    best = bestPop(pop)
    print(" ")
    prinFunction(best.perm, dictPositions, words)
    print(best.perm)
    print("fit=", best.fit)
    print(index)

    import matplotlib.pyplot as plt
    plt.plot(points)
    plt.ylabel('some numbers')
    plt.show()


def defineProbability(population):
    probabilitati = []
    totalFitness = 0
    for p in population:
        totalFitness += p.fit
    for chromosome in population:
        prob = np.interp(chromosome.fit, [0, totalFitness], [0, 1])
        probabilitati.append(prob)

    probabilitati /= sum(probabilitati)
    return probabilitati


def selectionV2(population, probabilitati):
    x = np.random.choice(population, 1, p=probabilitati)
    return x[0]


def SteadyState(sizePop, nrIterations, words, dictPositions, first, letters):
    """
    Steady State Algorithm

    :param sizePop: the size of the population
    :param nrIterations: the number of the iterations
    :param words: the list of words
    :param dictPositions:  the dictionary with the positions of letters
    :param first: the string with the first letters
    :param letters: the distinct letters
    :return: None
    """
    pop = init(sizePop, first, words, dictPositions)
    best = bestPop(pop)
    index = 0
    points = []
    while index < nrIterations and best.fit != 0:
        index += 1
        m = selectie(pop)
        t = selectie(pop)
        off = crossover(m, t)
        mutation(off, dictPositions, first)
        validate(off.perm, dictPositions, first)
        off.eval(words, dictPositions)
        worst = worstPop(pop)
        checkIndivid = True
        ind = 0
        while ind < len(pop):
            if pop[ind].perm == off.perm:
                checkIndivid = False
            ind += 1
        if checkIndivid == True:
            if pop[worst].fit > off.fit:
                pop[worst] = off
        else:
            index -= 1
        best = bestPop(pop)
        print(best.fit)
        if index % 10 == 0:
            points.append(best.fit)
    best = bestPop(pop)
    print(" ")
    if best.fit == 0:
        prinFunction(best.perm, dictPositions, words)
        makeGrafic(points)
        writeResultToFile(best.perm, dictPositions, letters)
    print(best.perm)
    print("fit=", best.fit)
    print(index)


def SS(nameFile, sizePop, nrIterations):
    """
    wrapper for the steady state algorithm

    :param nameFile:
    :param sizePop:
    :param nrIterations:
    :return:
    """
    letters = parserFile(nameFile)
    words = parsWords(nameFile)
    dictPositions = parserDict(nameFile)
    first = calcFirst(nameFile)
    SteadyState(sizePop, nrIterations, words, dictPositions, first, letters)
