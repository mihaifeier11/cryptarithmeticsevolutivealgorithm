import sys
import time

import matplotlib.pyplot as plt

from algorithms import *
from utils import *

inputFile   = sys.argv[1]
outputFile  = sys.argv[2]
generations = int(sys.argv[3])
population  = int(sys.argv[4])
algorithm   = sys.argv[5]

number, equation, firstElementsLettersList = readInput("input.txt")
dictionary, numbersList, firstElementsIndexList = createDictionary(equation, firstElementsLettersList)
startTime = time.time()

if algorithm == "steadyState":
    result, bestFitnessList = steadyState(generations, population, number, equation, dictionary, numbersList, firstElementsIndexList)
elif algorithm == "generational":
    result, bestFitnessList = generational(generations, population, number, equation, dictionary, numbersList, firstElementsIndexList)
else:
    raise Exception("Algorithm not found")

endTime = time.time()

printSolution(dictionary, result.getRepresentation())
print(result.getFitness())
printToFile(outputFile, dictionary, result.getRepresentation())

print("Operation took {}s.".format(round(endTime - startTime, 4)))

plt.plot(bestFitnessList)
plt.show()