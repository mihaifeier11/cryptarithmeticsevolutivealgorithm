class Chromosome:
    def __init__(self, number, equation, dictionary, representation):
        self.__number = number
        self.__equation = equation
        self.__dictionary = dictionary
        self.__representation = representation
        self.__fitness = 0
        self.__actualValue = 0
        self.__expectedValue = 0
        self.__maxFitness = pow(10, len(equation[-1]))

    def evaluate(self):
        actualResult = 0
        expectedResult = 0
        number = self.__number

        for line in self.__equation:
            value = 0
            for element in line:
                value = value * 10 + self.__representation[self.__dictionary[element]]

            if number > 0:
                actualResult += value
                number -= 1
            else:
                expectedResult = value

        self.__actualValue = actualResult
        self.__expectedValue = expectedResult

        self.__fitness = self.__maxFitness - abs(actualResult - expectedResult)

    def getNumber(self):
        return self.__number

    def getEquation(self):
        return self.__equation

    def getDictionary(self):
        return self.__dictionary

    def getActualValue(self):
        return self.__actualValue

    def getExpectedValue(self):
        return self.__expectedValue

    def getMaxFitness(self):
        return self.__maxFitness

    def isSolution(self):
        return self.__fitness == self.__maxFitness

    def isOver(self):
        return self.__actualValue > self.__expectedValue

    def getFitness(self):
        return self.__fitness

    def getRepresentation(self):
        return self.__representation

    def setRepresentationAtIndex(self, index, value):
        self.__representation[index] = value

    def setRepresentation(self, representation):
        self.__representation = representation
