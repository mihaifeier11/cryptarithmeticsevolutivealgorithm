import random

import numpy as np

from Chromosome import Chromosome
import copy

def defineProbability(population, totalFitness):
    """
    defines the probability of each chromosome of the population being chosen based on their fitness
    :param population:      the population containing the chromosomes
    :param totalFitness:    the total fitness of the population
    :return:
        @probabilities - a list containing the probability of each chromosome being chosen
    """
    probabilities = []
    for chromosome in population:
        probability = np.interp(chromosome.getFitness(), [0, totalFitness], [0, 1])
        probabilities.append(probability)

    probabilities /= sum(probabilities)
    return probabilities


def selection(population, probabilities):
    """
    selects a chromosome from the population based on the probabilities given
    :param population:      the population containing the chromosomes
    :param probabilities:   a list containing the probability of each chromosome being chosen
    :return:
        @chromosome - a random chromosome
    """
    x = np.random.choice(population, 1, p=probabilities)
    x = x[0]
    chr = Chromosome(x.getNumber(), x.getEquation(), x.getDictionary(), x.getRepresentation()[:])

    return chr


def isValid(list, firstElements):
    """
    checks if the list contains 0 for any index given in the firstElements list
    :param list:            the list of values to be checked
    :param firstElements:   the index of the first letters in each word
    :return:
        @True   - if there is no 0 in the position of a first elements
        @False  - otherwise
    """
    for index in firstElements:
        if list[index] == 0:
            return False

    return True


def generation(number, equation, dictionary, populationNumber, list, firstElements):
    """
    creates a generation of chromosomes
    :param number:              the number of lines to be added
    :param equation:            a list of list containing every line of the equation
    :param dictionary:          the dictionary with the letter as the key and the index for the list as the value
    :param populationNumber:    the number of genes every chromosome should have
    :param list:                the starting list from which a generation is created
    :param firstElements:       the index of the first letters in each word
    :return:
        @population - the population containing a generation
    """
    population = []

    for i in range(populationNumber):
        newList = list[:]
        random.shuffle(newList)
        while not isValid(newList, firstElements):
            random.shuffle(newList)

        chr = Chromosome(number, equation, dictionary, newList)
        population.append(chr)

    return population


def mutation(chromosome, dictionary, firstElements):
    """
    performs a valid mutation of the chromosome
    :param chromosome:      the chromosome on which the operation is performed
    :param dictionary:      the dictionary with the letter as the key and the index for the list as the value
    :param firstElements:   the index of the first letters in each word
    :return:
        @list - the list containing the mutation
    """
    list = chromosome.getRepresentation()
    length = len(list)
    r1 = random.randrange(length)
    r2 = random.randrange(length)

    while True:
        while r1 in dictionary.keys() and r2 in dictionary.keys():
            r1 = random.randrange(length)

        list[r1], list[r2] = list[r2], list[r1]
        if isValid(list, firstElements):
            break

    return list


def evaluation(population):
    """
    calls the evaluate function on every chromosome of the population
    :param population: the population containing the chromosomes
    :return:
        @best           - the best chromosome from the population
        @totalFitness   - the total fitness of the chromosomes from the population
    """
    best = population[0]
    totalFitness = 0
    for chromosome in population:
        chromosome.evaluate()
        totalFitness += chromosome.getFitness()
        if best.getFitness() < chromosome.getFitness():
            best = chromosome

    return best, totalFitness


def worst(population):
    """
    finds the worst chromosome from the population
    :param population: the population containing the chromosomes
    :return:
        @worst - the worst chromosome from the population
        @index - the index of the chromosome
    """
    worst = population[0]
    index = 0
    for i in range(len(population)):
        if worst.getFitness() > population[i].getFitness():
            worst = population[i]
            index = i

    return worst, index
